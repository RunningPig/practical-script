#!/bin/bash
#
# $1: gid
# $2: 文件数量
# $3: 文件路径

[[ "$2" -eq 0 ]] && exit 0

path="$3"
download_path="/root/Downloads"  # aria2 下载路径
dest_path="gdrive:"  # 远程目录

while true; do
  parent_path="${path%/*}"
  if [[ "${parent_path}" = "${download_path}" ]]; then
    source_path="${path}"
    [[ "$2" -eq 1 ]] || dest_path="${dest_path}/${source_path##*/}"
    rclone move "${source_path}" "${dest_path}" --delete-empty-src-dirs
    [[ "$2" -eq 1 ]] || rmdir "${source_path}"
    exit 0
  fi
  path="${parent_path}"
done

